import dash
from dash import dcc
from dash import html
from dash.dependencies import Output, Input, State
import plotly.graph_objects as go
import base64
import numpy as np
import cnc_clipper
import svgwrite
import json

external_stylesheets = [
    'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css',
]

uploader = html.Div(
    [
        html.Div(
            dcc.Upload(
                id='upload-data',
                children=['Drag and Drop or ', html.A('Select Files')],
                style={
                    'width': '100%',
                    'height': '60px',
                    'lineHeight': '60px',
                    'borderWidth': '1px',
                    'borderStyle': 'dashed',
                    'borderRadius': '5px',
                    'textAlign': 'center',
                    'margin': '10px'
                },
                # Allow multiple files to be uploaded
                multiple=False),
            className="col")],
    className="row")

diagram = html.Div(
    html.Div(dcc.Graph(id='diagram'), className="col"),
    className="row")

def make_row(label, id, placeholder=None, default=None):
    label = html.B(html.Label(label))
    inp = dcc.Input(id=id, type="number", placeholder=placeholder, value=default)
    results = (
        html.Div([label], className="col"),
        html.Div([inp], className="col"),
        html.Div(className="w-100"),
        )
    return results

def make_dropdown(label, id, default, labels, values, clearable=False):
    label = html.B(html.Label(label))
    options = [{'label': lab, 'value': val}
               for lab, val in zip(labels, values)]
    inp = dcc.Dropdown(id=id, value=default, options=options,
                       clearable=clearable)
    results = (
        html.Div([label], className="col"),
        html.Div([inp], className="col"),
        html.Div(className="w-100"),
        )
    return results

settings = html.Div(
    [
        *make_dropdown('units:', 'units', default="in", labels=["in", "mm"], values=["in", "mm"]),
        *make_row('scale:', 'scale-id', default=0.02),
        *make_row('diameter:', 'diameter-id', default=0.130),
        *make_row('margin:', 'margin-id', default=0.0),
    ],
    className="row"
)


download_buttons = html.Div(
    [
        html.B(html.Label("Download results:"), className="col"),
        html.Div([html.Button("CSV", id="button-csv"), dcc.Download(id="download-csv")], className="col"),
        html.Div([html.Button("SVG", id="button-svg"), dcc.Download(id="download-svg")], className="col"),
        html.Div([html.Button("JSON", id="button-json"), dcc.Download(id="download-json")], className="col"),
        html.Div([html.Button("PDF", id="button-pdf"), dcc.Download(id="download-pdf")], className="col"),
    ],
    className="row")

download_example = html.Div(
    [
        html.B(html.Label("Download Example:"), className="col"),
        html.Div([html.Button("LIZARD", id="button-lizard"), dcc.Download(id="download-lizard")], className="col"),
    ],
    className="row")

directions_= (
    html.P('Getting started:  Click on the \'LIZARD\' button to the right.  This downlads a CSV file that contains a tessellatable Esche lizard.  Then drag and drop that file up above here where it says "Drag and Drop or Select Files".'),
    html.P('The lizard should appear to the right.'),
    html.P('Then, adjust the scale, diameter, and margin to create a CNC routable vector with minimum internal and external radii so that the thing can be machine and all fit together.'),
    html.P("The diameter is the diameter of your cutting tool.  You should probably make this a little larger than your actual tool, or whatever diameter you like the looks of so that you won't have any problems."),
    html.P("The margin is a little inset if you'd like to leave a gap between each object."),
    html.P("Play around with the nubmers and it should work okay."),
)
directions = html.Div(directions_, className="row")
coordinates = html.Div([dcc.Textarea(value="coord area", id='coordinates-display')], className="row")
right_half_plane = html.Div([dcc.Graph(id="plot-area"), download_buttons, download_example], className="col")

left_half_plane = html.Div([uploader, settings, coordinates, directions], className="col")

the_store = dcc.Store(id='coordinate-store')

layout = html.Div([left_half_plane, right_half_plane, the_store], className="row")

plotter_layout = html.Div(layout, className="container")

app = dash.Dash(__name__, external_stylesheets=external_stylesheets, prevent_initial_callbacks=True)
app.layout = plotter_layout
server=app.server

def all_positive(coords):
    """Shift's coordinates to be all positive, so the no 
    values are negative."""
    coords = coords * 1.0 # make a copy
    coords[:, 0] -= np.min(coords[:, 0])
    coords[:, 1] -= np.min(coords[:, 1])
    return coords


def initialize_store(store):
    if store is None:
        store = json.loads(open("default.json").read())
    return store


@app.callback(
    [
        Output('plot-area', 'figure'),
        Output('coordinate-store', 'data'),
    ],
    [
        Input('upload-data', 'contents'),
        Input('scale-id', 'value'),
        Input('diameter-id', 'value'),
        Input('margin-id', 'value'),
    ],
    [
        State('upload-data', 'filename'),
    ]
)
def update_output(list_of_contents, scale, diameter, margin, list_of_fn):
    if scale is None:
        scale = 1.0
    if diameter is None:
        diameter = 0.25
    if margin is None:
        margin = 0

    fig = go.Figure()
    coords = []
    text_to_display = "Waiting"
    store = None
    if list_of_contents is not None:
        content_type, content_string = list_of_contents.split(",")
        text_to_display = content_string
        txt = base64.b64decode(content_string).decode("utf-8")
        rows = txt.split("\n")
        for row in rows:
            try:
                x, y = row.split(",")
                coords.append((float(x), float(y)))
            except ValueError:
                pass

    fig.update_layout(title="What you got?",
                      xaxis_title="x",
                      yaxis_title="y",
                      )
    coords = np.array(coords)
    text_to_display = str(coords.shape)
    final_shape = None
    orig_shape = None
    if (len(coords.shape) == 2):
        coords = coords * scale
        coords = cnc_clipper.vclose(coords)
        orig_shape = all_positive(coords)
        x = orig_shape[:, 0]
        y = orig_shape[:, 1]
        fig.add_trace(go.Scatter(x=x, y=y, fill="toself"))

        try:
            b = cnc_clipper.do_cnc(orig_shape, diameter/2)
            b = cnc_clipper.upordown(b, -margin)
            x = b[:, 0]
            y = b[:, 1]
            final_shape = b
            fig.add_trace(go.Scatter(x=x, y=y, fill="toself"))
            store = (orig_shape, final_shape)
        except IndexError:
            pass
    return fig, store


@app.callback(
    Output("download-csv", "data"),
    Input('button-csv', "n_clicks"),
    State('coordinate-store', 'data'),
    prevent_initial_call=True,
)
def download_csv(n_clicks, store):
    if store is None:
        return
    orig_shape, final_shape = store
    if final_shape is None:
        return None
    results = []
    for x, y in orig_shape:
        results.append(f"{x},{y}")
    results = "\n".join(results)
    return dict(content=results, filename="cnc.csv")


@app.callback(
    Output("download-svg", "data"),
    [Input('button-svg', "n_clicks"),
     Input('units', 'value')],
    State('coordinate-store', 'data'),
    prevent_initial_call=True,
)
def download_svg(n_clicks, units, store):
    if store is None:
        return
    ppi = 96
    orig_shape, final_shape = store
    orig_shape = np.array(orig_shape, dtype=float) * ppi
    final_shape = np.array(final_shape, dtype=float) * ppi
    #final_shape = np.array(([0, 0], [0, 1000], [1000, 1000], [1000, 0], [0, 0]), dtype=float)
    min_x = np.min(orig_shape)
    max_x = np.max(orig_shape)
    min_y = np.min(orig_shape)
    max_y = np.max(orig_shape)
    width = max_x - min_x
    height = max_y - min_y
    dwg = svgwrite.Drawing("cnc.svg",
                           size=(width, height),
                           profile="full",
                           )
    dwg.add(dwg.polygon(points=orig_shape,
                        fill='red', opacity=0.25))
    dwg.add(dwg.polygon(points=final_shape,
                        fill='green', opacity=0.25))
    results = dwg.tostring()
    return dict(content=results, filename="cnc.svg")

@app.callback(
    Output("download-lizard", "data"),
    Input('button-lizard', "n_clicks"),
    prevent_initial_call=True,
)
def download_svg(n_clicks):
    return dict(content=open("lizard.csv").read(), filename="lizard.csv")

if __name__ == '__main__':
    app.run_server(debug=True)
