import pyclipper
import numpy as np


def vclose(x):
    last = x[0, :]
    last = np.expand_dims(last, 0)
    r = np.concatenate((x, last))
    return r


def get_rescaled(x, offset):
    pco = pyclipper.PyclipperOffset()
    pco.AddPath(x, pyclipper.JT_ROUND, pyclipper.ET_CLOSEDPOLYGON)
    solution = pco.Execute(offset)
    solution=solution[0]
    solution = np.array(solution)
    return solution


def downup(x, offset):
    return updown(x, -offset)


def calculate_scale(x):
    """find a good scale for x.  for some reason the clipper library needs points 
    that are in int16 I think"""
    big = np.max(x)
    little = np.min(x)
    scale = np.max(np.abs((big, little)))
    # scale it to about 20000
    scale = 20000/scale
    return scale


def updown(x, offset):
    scale = calculate_scale(x)
    x = np.array(x * scale, dtype=int)
    offset = offset * scale
    a = get_rescaled(x, offset)
    b = get_rescaled(a, -offset)
    b = np.array(b, dtype=float)/scale
    a = np.array(a, dtype=float)/scale
    return b

def upordown(x, offset):
    scale = calculate_scale(x)
    x = np.array(x * scale, dtype=int)
    offset = offset * scale
    a = get_rescaled(x, offset)
    a = np.array(a, dtype=float)/scale
    return a
    
def do_cnc(x, offset):
    return updown(downup(x, offset), offset)


def rotatePolygon(polygon, angle, center_point):
    """ Rotate polygon the given angle about its center. """
    theta = np.radians(angle)
    cosang, sinang = np.cos(theta), np.sin(theta)

    points = polygon
    # find center point of Polygon to use as pivot
    n = points.shape[0]
    cx, cy = center_point

    new_points = []
    for x, y in points:
        tx, ty = x-cx, y-cy
        new_x = ( tx*cosang + ty*sinang) + cx
        new_y = (-tx*sinang + ty*cosang) + cy
        new_points.append((new_x, new_y))
    return np.array(new_points)

