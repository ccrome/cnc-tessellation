#Grab the latest alpine image
FROM continuumio/miniconda3

RUN apt update
COPY webapp/requirements.txt conda-requirements.txt /tmp/

RUN conda install --file /tmp/conda-requirements.txt

ADD ./webapp /opt/webapp/
WORKDIR /opt/webapp

# Expose is NOT supported by Heroku
# EXPOSE 5000 		

RUN pip install -r /opt/webapp/requirements.txt

# Run the image as a non-root user
RUN useradd -ms /bin/bash myuser
RUN chmod -R +r /opt/webapp/*
USER myuser


# Run the app.  CMD is required to run on Heroku
# $PORT is set by Heroku			
CMD gunicorn --bind 0.0.0.0:$PORT app:server
