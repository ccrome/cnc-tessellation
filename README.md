# CNC Object fitter.
This is a little CNC helper project to help with tessellations and
other projects that require CNC milling with finate curve radius.

See this example running live [here](https://cnc-object-fitter.herokuapp.com/)

# Instructions for running this heroku application
This runs from a docker.

The docker is built from anaconda python, continuumio/miniconda3


* Check out this directory from gitlab:  `git clone git@gitlab.com:ccrome/cnc-tessellation.git`
* create a heroku application:  `heroku create <my-application-name>`.  This creates an application and puts it into your git as a remote.  You can see what's what by typing `git remote -v`, and you should see something like `heroku  https://git.heroku.com/cnc-tessellation.git (fetch)`
* push this applicaiton to heroku.  This pushes the docker only I think: `heroku container:push web`
* release this container: `heroku container:release web`
* Then watch the log files: `heroku logs`
* Once it's started, you can open the remote application with `heroku open`

Whenever you update your project, commit it all, then do a `container:push` and `container:release`

**You need to force a rebuild of the Docker each time you push.  Not
  sure how this is supposed to work, but for now, be sure your docker
  rebuilds each time you change any python.**
